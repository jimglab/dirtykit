#!/usr/bin/env python
# -*- coding: utf-8 -*-
import funcs as ff
import argparse

#--- retrieve args
parser = argparse.ArgumentParser(
formatter_class=argparse.ArgumentDefaultsHelpFormatter
)
parser.add_argument(
'-ip', '--ip_addr',
type=str,
default='',
help='input filename of ACE data',
)
parser.add_argument(
'-u', '--user',
type=str,
default='',
)
parser.add_argument(
'-p', '--pasw',
type=str,
default='',
)
parser.add_argument(
'-cmd', '--cmd',
type=str,
default='',
)
parser.add_argument(
'-wait', '--wait',
type=int,
default=0,
)
parser.add_argument(
'-ask', '--askpass',
action='store_true',
default=False,
)
parser.add_argument(
'-entryp', '--entrypoint',
type=str,
default='',
)
pa = parser.parse_args()

assert (pa.ip_addr!='' and pa.user!='')

# log-in && execute command inside the remote
ss = ff.ssh(
    host=pa.ip_addr, 
    execute=pa.cmd,
    askpass=pa.askpass, 
    user=pa.user, 
    password=pa.pasw,
    )
ss.run(wait=pa.wait, entryp=pa.entrypoint)


#EOF
