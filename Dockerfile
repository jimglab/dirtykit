FROM ubuntu:12.04

RUN apt-get update && apt-get upgrade -y &&\
    apt-get install -y openssh-server
RUN useradd -m -G sudo -s /bin/bash victim &&\
    useradd -m -s /bin/bash toor

CMD ["bash"]
