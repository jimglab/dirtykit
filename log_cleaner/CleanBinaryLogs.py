#!/usr/bin/python
import struct
import sys, shutil, pwd, os, time
from datetime import datetime, timedelta
import funcs
import argparse

# retrieve the IDentifiers
parser = argparse.ArgumentParser(
formatter_class=argparse.ArgumentDefaultsHelpFormatter
)
parser.add_argument(
'-u', '--user', 
type=str,
help='user that logged-in',
)
parser.add_argument(
'-ho', '--host', 
type=str,
help='host from which the user logged-in',
)
parser.add_argument(
'-since', '--since', 
type=int,
help='number of seconds since which we will delete records.',
)
pa = parser.parse_args()

userToRemove = pa.user 
hostToRemove = pa.host 
# get time time (in local time) of right now!
dstruct_now  = time.localtime(time.time())
dd, mm, yyyy = dstruct_now.tm_mday, dstruct_now.tm_mon, dstruct_now.tm_year
HH, MM       = dstruct_now.tm_hour, dstruct_now.tm_min
# date since which the records will be erased
sdate        = datetime(yyyy, mm, dd, HH, MM) - timedelta(seconds=pa.since)
print "\n [*] erasing for user %s since %s\n"%(pa.user, sdate.strftime('%d/%m/%Y %H:%M'))

# Just do it!
ifiles = {
'UTMP_FILEPATH'     : '/var/run/utmp', #pa.srcdir + "/utmp",
'WTMP_FILEPATH'     : '/var/log/wtmp', #pa.srcdir + "/wtmp",
'LASTLOG_FILEPATH'  : '/var/log/lastlog', #pa.srcdir + "/lastlog"
}
assert all([ os.path.isfile(_) for _ in ifiles.values() ])
lc = funcs.logcleaner(userToRemove, hostToRemove, sdate, ifiles)
lc.wipeit()

#EOF
