# Adapted from:
# https://github.com/mr-sk/pyLogCleaner.git
#
# Remove the specified username/host entry from the /var/run/utmp,
# /var/log/wtmp and /var/log/lastlog files. Must be privileged for wtmp/lastlog.
# The data in the these files is stored as binary which makes it a little
# trickier than a traditional ascii text file.
#
# The utmp struct size was determined via a sizeof(struct utmp) call
# in C. This could change on different archs!
# The size of a lastlog entry is:
#
# struct lastlog
# {
# #if __WORDSIZE == 64 && defined __WORDSIZE_COMPAT32
# int32_t ll_time;
# #else
# __time_t ll_time; // 4
# #endif
# char ll_line[UT_LINESIZE]; // 32
# char ll_host[UT_HOSTSIZE]; // 256
# };
# which is 292 bytes.
#
# What's interesting about lastlog is the corelation between user id and
# entry in the lastlog file - I guess it makes sense due to the sparse
# format of the lastlog file and all the extraneous "empty" entries.
# Regardless, we have to find the id index for the user and remove THAT entry.
# Entries appear as:
# (14009, 20242, 'pts/6\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\
# x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00',
# ....
# x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00')
#
# Sources:
# /usr/include/x86_64-linux-gnu/bits/utmp.h
# /usr/include/utmp.h
# -----------------------------------------------------------------------------
import struct
import sys
import shutil
import pwd
from pyutmp import test
import os
from datetime import datetime
import time


# You *might* have to change these:
# Tested on Linux ubuntu 3.0.0-12-generic #20-Ubuntu
UTMP_STRUCT_SIZE = 384
LASTLOG_STRUCT_SIZE = 292
# time format DERIVED from the output of time.ctime()
DB_TIME_FORMAT      = '%b %d %H:%M:%S %Y'


# strip binary strings
cut = lambda s: str(s).split("\0",1)[0]


# Writes a binary file.
#
# filePath The fullpath w/filename and extension to read from
# fileContents The contents to be written to 'filePath'
def writeNewFile(filePath, fileContents):
  f = open(filePath, "w+b")
  f.write(fileContents)
  f.close()


class f2iter(object):
    """
    iterate over the contents of a file, with a
    discrete step given by `sbytes` (see self.next()).
    """
    def __init__(self, fname, mode='rb', sbyte=1):
        self.fname  = fname
        self.f      = open(fname, mode)
        self.sbyte  = sbyte

    def __iter__(self):
        return self

    def next(self):
        """
        read at most `sbyte` bytes
        """
        bytes = self.f.read(self.sbyte)
        if bytes!="":
            return bytes
        else:
            print " --> We reached the end of: "+self.fname+"!"
            raise StopIteration

    def close(self):
        print " --> Closing: "+self.fname
        self.f.close()


class logcleaner(object):
    def __init__(self, rm_user, rm_host, sdate, ifiles):
        self.ifiles = ifiles
        self.UTMP_FILEPATH = self.ifiles['UTMP_FILEPATH']
        self.WTMP_FILEPATH = self.ifiles['WTMP_FILEPATH']
        self.LASTLOG_FILEPATH = self.ifiles['LASTLOG_FILEPATH']

        self.rm_user = rm_user
        self.rm_host = rm_host
        self.sdate   = sdate

    def wipeit(self,):
        #--- wipe utmp
        _ = self.wipe_tmp(self.UTMP_FILEPATH)
        writeNewFile(self.UTMP_FILEPATH, _)

        #--- wipe wtmp
        _ = self.wipe_tmp(self.WTMP_FILEPATH)
        writeNewFile(self.WTMP_FILEPATH, _)

        #--- wipe LastLog
        #_ = self.wipe_lastlog()
        #writeNewFile(self.LASTLOG_FILEPATH, _)


    def wipe_tmp(self, filepath):
        """
        Process the utmp && wtmp.
        - all entries that satisfy the following are cleaned
            - matching the user `usernameToRemove`
            - with host == hostAddressToRemove
            - with time/date after `sdate`
        """
        print " --> Checking... "+filepath; #raw_input()
        newUtmp = ""
        # the raw way
        f_it = f2iter(filepath, mode='rb', sbyte=UTMP_STRUCT_SIZE)
        # using Cython interface
        f_c  = test._UtmpFile(path=filepath)
        print " name :: host :: date-time "
        for bytes, _centry in zip(f_it, f_c):
            #-- let's get the fucking entry's time/date
            _time_str = time.ctime(_centry.ut_time)[4:]
            _time     = datetime.fromtimestamp(time.mktime(time.strptime(_time_str, DB_TIME_FORMAT)))
            _c_name = _centry.ut_user
            _c_host = _centry.ut_host # host from which the user logs-in

            #-- raw bytes
            data = struct.unpack("hi32s4s32s256shhiii36x", bytes)
            _name = cut(data[4])
            _host = cut(data[5])
            #print "*****"
            #print _name, _host
            #print _c_name, _c_host
            #raw_input()
            print _name,'::',_host,'::',_time

            #if _name == self.rm_user: 
            #    print " >> ", self.rm_user; raw_input()

            #-- Clean==True if this entry is "clean"
            Clean = (_name != self.rm_user)
            Clean |= (_host != self.rm_host)
            Clean |= (_time < self.sdate)
            if Clean:
                newUtmp += bytes

        f_it.close()
        return newUtmp

    def wipe_lastlog(self,):
        """
        Process the 'lastlog' file
        """
        print " --> Checking lastlog... "; #raw_input()
        pw = pwd.getpwnam(self.rm_user)
        uid= pw.pw_uid
        idCount = 0
        newLastlog = ''

        f_it = f2iter(self.LASTLOG_FILEPATH, mode='rb', sbyte=LASTLOG_STRUCT_SIZE)
        for bytes in f_it:
            data = struct.unpack("hh32s256s", bytes)
            print " idCount/uid = ", idCount, "/", uid
            if (idCount != uid):
                newLastlog += bytes
            else:
                print " MY DATA: ", data # show me what's on my registry!
                print " len(data): ", len(data)
                print " > data[0]: ", data[0]
                print " > data[1]: ", data[1]
                print " > data[2]: ", data[2]
                print " > data[3]: ", data[3]
            idCount += 1

        f_it.close()
        return newLastlog


#EOF
