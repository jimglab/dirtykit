#!/usr/bin/env ipython
# -*- coding: utf-8 -*-
"""
source:
https://stackoverflow.com/questions/21433660/how-to-interact-with-ssh-using-subprocess-module#21433723
"""
import pty, sys
from subprocess import Popen, PIPE, STDOUT
from time import sleep
from os import fork, waitpid, execv, read, write, kill
import signal

class ssh(object):
    def __init__(self, host, execute='echo "done" > /root/testing.txt', askpass=False, user='root', password=b'SuperSecurePassword'):
        self.execc = execute
        self.host = host
        self.user = user
        self.password = password
        self.askpass = askpass

    def run(self, wait=0, entryp=''):
        """
        wait    : seconds to wait before sending a SIGINT kill signal
        """
        if entryp=='':
            command = [
            '/usr/bin/ssh',
            self.user+'@'+self.host,
            '-o', 'NumberOfPasswordPrompts=1',
            self.execc,
            ]
        else:
            command = []
            command += entryp.split()
            command += [ self.execc ]

        # PID = 0 for child, and the PID of the child for the parent    
        pid, child_fd = pty.fork()

        if not pid: # Child process
            # Replace child process with our SSH process
            execv(command[0], command)

        # if we havn't setup public-key authentication
        # we can loop for a password promt and "insert" the password.
        while self.askpass:
            try:
                output = read(child_fd, 1024).strip()
            except:
                break
            lower = output.lower()
            # Write the password
            if b'password:' in lower:
                write(child_fd, self.password + b'\n')
                break
            elif b'are you sure you want to continue connecting' in lower:
                # Adding key to known_hosts
                write(child_fd, b'yes\n')
            elif b'company privacy warning' in lower:
                pass # This is an understood message
            else:
                print('Error:',output)

        print "\n [*] Executing:\n %s\n" % self.execc
        if wait > 0:
            print " [*] waiting %d sec to kill PID:%d\n"%(wait, pid)
            sleep(wait)
            print " [*] killing... "
            kill(pid, signal.SIGINT)
        else:
            waitpid(pid, 0)
        return True

#EOF
